function add() {
  try {
    var a = prompt("Insert the value of 'a': ", "");
    if (!a || isNaN(a))
      throw new Error("Invalid value of a: " + a)
    var b = prompt("Insert the value of 'b': ", "");
    if (!b || isNaN(b))
      throw new Error("Invalid value of b: " + b)
    
    var c = parseInt(a) + parseInt(b);
    alert("The sum is:  " + c);
  } catch (e) {
    alert("The error is: " + e.message);
  }
}